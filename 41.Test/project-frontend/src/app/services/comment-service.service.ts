import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { Filter } from "../models/Filter";
import { News } from "../models/News";
import { Comment } from "../models/Comment";

@Injectable()
export class CommentService {
  constructor(private http: HttpClient) { }

  getByNewsId(id: number): Observable<Comment[]> {
    return this.http.get(`api/commentByNewsId/${id}`) as Observable<Comment[]>;
  }

  getByCommentId(id: number): Observable<Comment[]> {
    return this.http.get(`api/commentByCommentId/${id}`) as Observable<Comment[]>;
  }

  save(comment: Comment): Observable<Comment> {
    let params = new HttpParams();
    params = params.append("Content-Type", "application/json");
    return comment.id === undefined ?
      this.http.post(`api/comments`, comment, { params }) as Observable<Comment>
      : this.http.put(`api/comments/${comment.id}`, comment, { params }) as Observable<Comment>;

  }
}
