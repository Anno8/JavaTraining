import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { Filter } from "../models/Filter";
import { News } from "../models/News";

@Injectable()
export class NewsService {
  private readonly path = "api/news";
  constructor(private http: HttpClient) { }

  getAll(page: number, itemsPerPage: number, filter: Filter): Observable<Page<News>> {
    let params = new HttpParams();
    params = params.append("page", page.toString());
    params = params.append("size", itemsPerPage.toString());
    params = params.append("name", filter.name || "");
    params = params.append("categoryId", filter.categoryId.toString());

    return this.http.get(this.path, { params: params }) as Observable<Page<News>>;
  }

  get(id: number): Observable<News> {
    return this.http.get(`${this.path}/${id}`) as Observable<News>;
  }

  save(comp: News): Observable<News> {
    let params = new HttpParams();
    params = params.append("Content-Type", "application/json");
    return comp.id === undefined ?
      this.http.post(this.path, comp, { params }) as Observable<News>
      : this.http.put(`${this.path}/${comp.id}`, comp, { params }) as Observable<News>;

  }

  delete(comp: News): any {
    return this.http.delete(`${this.path}/${comp.id}`);
  }
}
