import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { HttpParams } from "@angular/common/http/src/params";
import { Category } from "../models/Category";

@Injectable()
export class CategoryService {
    private readonly path = "api/categories";
    constructor(private http: HttpClient) { }

    getAll(): Observable<Category[]> {
        return this.http.get(this.path) as Observable<Category[]>;
    }

}
