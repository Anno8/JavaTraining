import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { ListComponent } from "./components/list/list.component";
import { EditComponent } from "./components/edit/edit.component";
import { FilterComponent } from "./components/filter/filter.component";
import { DetailsComponent } from "./components/details/details.component";

import { AuthenticationService } from "./services/authentication-service.service";
import { JwtUtilsService } from "./services/jwt-utils.service";
import { TokenInterceptorService } from "./services/token-interceptor.service";
import { CanActivateAuthGuard } from "./services/can-activate-auth.guard";
import { CategoryService } from "./services/category-service.service";
import { NewsService } from "./services/news-service.service";
import { CommentService } from "./services/comment-service.service";

const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "list", component: ListComponent },
  // { path: "add", component: AddComponentComponent, canActivate: [CanActivateAuthGuard] },
  { path: "details/:id", component: DetailsComponent },
  { path: "", redirectTo: "list", pathMatch: "full" },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ListComponent,
    EditComponent,
    DetailsComponent,
    FilterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    CategoryService,
    NewsService,
    CommentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
