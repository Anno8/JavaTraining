import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import { Category } from "../../models/Category";
import { Page } from "../../models/Page";
import { News } from "../../models/News";
import { Filter } from "../../models/Filter";

import { NewsService } from "../../services/news-service.service";
import { CategoryService } from "../../services/category-service.service";
import { AuthenticationService } from "../../services/authentication-service.service";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent {
  availableCategories: Category[];
  page: Page<News>;
  currentPageNumber: number;
  totalPages: number;
  itemsPerPage = 10;

  // Initial value of the filter
  filter: Filter = {
    name: "",
    categoryId: 0
  }

  forEdit: News = {
    category: undefined,
    content: "",
    description: "",
    header: ""
  }


  constructor(
    private authenticationService: AuthenticationService,
    private newsService: NewsService,
    private categoryService: CategoryService,
    private router: Router) { }

  ngOnInit() {
    this.currentPageNumber = 0;
    this.loadData();
  }

  search(filter: Filter) {
    this.filter = filter
    this.newsService.getAll(this.currentPageNumber,
      this.itemsPerPage, this.filter).subscribe(data => {
        this.page = data;
        this.totalPages = data.totalPages;
      })
  }

  loadData() {
    this.search(this.filter);
    this.categoryService.getAll().subscribe(
      (categories) => {
        this.availableCategories = categories;
      }
    );
  }

  delete(n: News) {
    this.newsService.delete(n).subscribe(
      (res) => {
        this.loadData();
      },
      (err) => {
        this.loadData();
      }
    );
  }

  save(newsToSave: News) {
    this.newsService.save(newsToSave).subscribe(() => this.loadData());
  }

  itemsPerPageChanged(value: number): void {
    this.itemsPerPage = value;
    this.loadData();
  }

  changePage(i: number) {
    this.currentPageNumber += i;
    this.loadData();
  }

  isLoggedIn(): boolean {
    return this.authenticationService.isLoggedIn();
  }

  isAdmin(): boolean {
    return this.authenticationService.isAdmin();
  }

  edit(n: News) {
    this.forEdit = n;
  }
}

