import { Component, OnInit } from "@angular/core";

import { AuthenticationService } from "../../services/authentication-service.service";
import { Observable } from "rxjs/Observable";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "../../models/Page";
import { News } from "../../models/News";
import { NewsService } from "../../services/news-service.service";
import { CategoryService } from "../../services/category-service.service";
import { Category } from "../../models/Category";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class EditComponent {
  @Output()
  newsToSave: EventEmitter<News> = new EventEmitter();

  @Input()
  news: News;

  @Input()
  categories: Category[];

  reset() {
    this.news = {
      header: "",
      category: undefined,
      content: "",
      description: ""
    };
  }
  save() {
    this.newsToSave.next(this.news);
    this.reset();
  }

  byId(c1: Category, c2: Category) {
    if (c1 && c2) {
      return c1.id === c2.id;
    }
  }

}

