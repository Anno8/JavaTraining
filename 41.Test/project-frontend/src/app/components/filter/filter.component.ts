// import { Component, OnInit, ViewEncapsulation } from "@angular/core";
// import { Input } from "@angular/core";
// import { ComponentTypeService } from "../../services/componentType-service.service";
// import { Filter } from "../../models/Filter";
// import { Category } from "../../models/Category";
// import { CategoryService } from "../../services/category-service.service";

// @Component({
//   selector: "app-filter",
//   templateUrl: "./filter.component.html",
//   styleUrls: ["./filter.component.css"],
//   encapsulation: ViewEncapsulation.None
// })
// export class FilterComponent implements OnInit {
//   filter: Filter;
//   categories: Category[];
//   constructor(
//     private categoryService: CategoryService,
//     private componentTypeService: ComponentTypeService) {
//   }

//   ngOnInit() {
//     this.loadData();
//   }

//   loadData() {
//     this.categories.getAll().subscribe((data) => this.categories = data);
//     this.filter = {
//       name: "",
//       categoryId: 0,
//     };
//   }

//   selectionChanged() {
//     console.log(this.filter);
//   }
// }

import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";
import { Filter } from "../../models/FIlter";
import { CategoryService } from "../../services/category-service.service";
import { Category } from "../../models/Category";


@Component({
  selector: "app-filter",
  templateUrl: "./filter.component.html",
  styleUrls: ["./filter.component.css"],
})
export class FilterComponent implements OnInit {

  @Output() filterItems: EventEmitter<Filter> = new EventEmitter();

  filter: Filter = {
    name: "",
    categoryId: 0
  }
  categories: Category[];
  constructor(private categoryService: CategoryService

  ) {
  }

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this.categoryService.getAll().subscribe(data => {
      this.categories = data;
    }
    );

  }
  selectionChanged() {
    this.filterItems.next(this.filter);
  }
}
