import { Component, OnInit } from "@angular/core";

import { AuthenticationService } from "../../services/authentication-service.service";
import { Observable } from "rxjs/Observable";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "../../models/Page";
import { News } from "../../models/News";
import { Comment } from "../../models/Comment";
import { NewsService } from "../../services/news-service.service";
import { CommentService } from "../../services/comment-service.service";

@Component({
    selector: "app-details-component",
    templateUrl: "./details.component.html",
    styleUrls: ["./details.component.css"]
})
export class DetailsComponent {
    news: News;
    comments: Comment[];
    comment: Comment = {
        author: "",
        content: "",
    }
    childComment: Comment;

    constructor(
        private newsService: NewsService,
        private commentService: CommentService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        this.route.params.subscribe((params) => {
            let newsId = +params["id"];
            this.newsService.get(newsId).subscribe(
                (data) => {
                    this.news = data;
                    this.comment.news = data;
                    this.commentService.getByNewsId(newsId).subscribe((data) => this.comments = data);
                },
                (error) => {
                    console.log(error);
                });
        });
    }
    commentOnComment(c: Comment) {
        this.childComment = {
            news: this.news,
            author: "",
            content: ""
        }
    }

    addCommentToComment(c: Comment) {
        c.comments.push(this.childComment);
        this.commentService.save(c).subscribe(() => {
            this.loadData();
        });
        this.childComment = undefined;
    }

    add() {
        this.commentService.save(this.comment).subscribe(() => {
            this.loadData();
            this.comment = {
                content: "",
                author: ""
            }
        });
    }
}

