import { Category } from "./Category";

export interface News {
    id?: number;
    header: string;
    category: Category;
    description: string;
    content: string;
}