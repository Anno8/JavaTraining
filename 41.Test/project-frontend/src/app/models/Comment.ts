import { News } from "./News";

export interface Comment {
    id?: number;
    author: string;
    content: string;
    news?: News
    comments?: Comment[];
}