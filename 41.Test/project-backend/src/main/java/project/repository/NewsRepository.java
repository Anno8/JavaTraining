package project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import project.model.News;

@Component
public interface NewsRepository extends JpaRepository<News, Long>{
	Page<News> getByHeaderContains(Pageable page, String name);
	Page<News> getByHeaderContainsAndCategoryId(Pageable page, String name, Long categoryId);
}
