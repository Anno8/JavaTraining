package project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import project.model.Comment;

@Component
public interface CommentRepository extends JpaRepository<Comment, Long> {
	List<Comment> getByNewsId(Long id);
	List<Comment> getByCommentsId(Long id);
}
