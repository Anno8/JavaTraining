package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import project.model.News;
import project.repository.NewsRepository;

@Component
public class NewsService {
	@Autowired
	private NewsRepository newsRepository;
	
	public Page<News> findAll(Pageable page, String name, Long categoryId) {
		if(categoryId != 0)
			return newsRepository.getByHeaderContainsAndCategoryId(page, name, categoryId);
		
		return newsRepository.getByHeaderContains(page, name);
	}
	
	public Page<News> findAll(Pageable page) {
		return newsRepository.findAll(page);
	}

	public News findOne(Long id) {
		return newsRepository.findOne(id);
	}

	public News save(News n) {
		return newsRepository.save(n);
	}

	public void remove(Long id) {
		newsRepository.delete(id);
	}
}
