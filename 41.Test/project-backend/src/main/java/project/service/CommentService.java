package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.model.Comment;
import project.repository.CommentRepository;
import project.repository.NewsRepository;

@Component
public class CommentService {
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private NewsRepository newsRepo;
	
	public Comment getById(Long id) {
		return commentRepository.getOne(id);
	}
	
	public List<Comment> getByNewsId(Long newsId){
		return commentRepository.getByNewsId(newsId);
	}
	
	public List<Comment> getByCommentId(Long commentId){
		return commentRepository.getByCommentsId(commentId);
	}
	
	public Comment save(Comment comment) {
		return commentRepository.save(comment);
	}
}
