package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.FilterDTO;
import project.model.News;
import project.service.NewsService;

@RestController
public class NewsController {
	@Autowired
	private NewsService newsService;

	@GetMapping(value = "api/news")
	public Page<News> get(Pageable pageable, String name, Long categoryId) {
		return newsService.findAll(pageable, name, categoryId);
	}

	@GetMapping(value = "api/news/{id}")
	public ResponseEntity<News> get(@PathVariable Long id) {
		return new ResponseEntity<News>(newsService.findOne(id), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@PostMapping(value = "api/news")
	public ResponseEntity<News> create(@RequestBody News n) {
		return new ResponseEntity<News>(newsService.save(n), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@DeleteMapping(value = "api/news/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
		News n = newsService.findOne(id);

		if (n == null)
			return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);

		newsService.remove(id);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@PutMapping(value = "api/news/{id}")
	public ResponseEntity<News> update(@PathVariable Long id, @RequestBody News n) {
		News res = newsService.findOne(id);

		if (res == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		res.setHeader(n.getHeader());
		res.setCategory(n.getCategory());
		res.setContent(n.getContent());
		res.setDescription(n.getDescription());
		
		return new ResponseEntity<News>(newsService.save(res), HttpStatus.OK);

	}
}
