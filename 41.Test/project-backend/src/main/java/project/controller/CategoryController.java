package project.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.CategoryDTO;
import project.service.CategoryService;

@RestController
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "api/categories")
	public ResponseEntity<List<CategoryDTO>> get() {
		return new ResponseEntity<List<CategoryDTO>>(categoryService.findAll().stream().map(CategoryDTO::new).collect(Collectors.toList()), HttpStatus.OK);
	}
}
