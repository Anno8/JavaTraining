package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.model.Comment;
import project.service.CommentService;

@RestController
public class CommentController {
	@Autowired
	private CommentService commentService;
	
	@GetMapping(value = "api/commentByNewsId/{id}")
	public ResponseEntity<List<Comment>> getByNewsId(@PathVariable Long id) {
		return new ResponseEntity<List<Comment>>(commentService.getByNewsId(id), HttpStatus.OK);
	}

	@GetMapping(value = "api/commentByCommentId/{id}")
	public ResponseEntity<List<Comment>> getByCommentId(@PathVariable Long id) {
		return new ResponseEntity<List<Comment>>(commentService.getByCommentId(id), HttpStatus.OK);
	}

	
	@PostMapping(value = "api/comments")
	public ResponseEntity<Comment> create(@RequestBody Comment comment) {
		return new ResponseEntity<Comment>(commentService.save(comment), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "api/comments/{id}")
	public ResponseEntity<Comment> update(@PathVariable Long id, @RequestBody Comment c) {
		Comment res = commentService.getById(id);

		if (res == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		res.setAuthor(c.getAuthor());
		res.setContent(c.getContent());
		res.setComments(c.getComments());
		
		return new ResponseEntity<Comment>(commentService.save(res), HttpStatus.OK);

	}
}
