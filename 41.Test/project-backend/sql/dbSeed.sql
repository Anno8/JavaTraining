create database project;
use project;

insert into security_user (username, password, role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin');
    
INSERT INTO category (name) VALUES('Life');
INSERT INTO category (name) VALUES('Sport');
INSERT INTO category (name) VALUES('Music');

INSERT INTO news (content, description, header, category_id) 
VALUES(
'Magic',
 'You better believe it', 
 'The shocking turth of how Trump became president.', 
  1);
INSERT INTO news (content, description, header, category_id) 
VALUES(
'Bla bla Blah Bla',
 'A short innovative description', 
 'Header of a intruging post.', 
  3);
  INSERT INTO news (content, description, header, category_id) 
VALUES(
'Music lalalala',
 'A short innovative description takes another twist', 
 'Music.', 
  3);
 INSERT INTO news (content, description, header, category_id) 
VALUES(
'Sport isnt what it used to be',
 'ESPORT scene is destroying the traditional sport', 
 'ESPORT.', 
  2);
  INSERT INTO news (content, description, header, category_id) 
VALUES(
'Magic',
 'You better believe it', 
 'The shocking turth of how Trump became president.', 
  1);
  INSERT INTO news (content, description, header, category_id) 
VALUES(
'Magic',
 'You better believe it', 
 'The shocking turth of how Trump became president.', 
  1);
  INSERT INTO news (content, description, header, category_id) 
VALUES(
'Magic',
 'You better believe it', 
 'The shocking turth of how Trump became president.', 
  1);
  INSERT INTO news (content, description, header, category_id) 
VALUES(
'Magic',
 'You better believe it', 
 'The shocking turth of how Trump became president.', 
  1);